class CreateTasks < ActiveRecord::Migration[6.0]
  def change
    create_table :tasks do |t|
      t.string :name
      t.text :memo
      t.date :endate
      t.string :prority
      t.string :status
      t.string :label

      t.timestamps
    end
  end
end
