json.extract! task, :id, :name, :memo, :endate, :prority, :status, :label, :created_at, :updated_at
json.url task_url(task, format: :json)
