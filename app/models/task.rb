class Task < ApplicationRecord
	belong_to :user
	validates :memo , length: { maximum: 140 }
	validates :name , presence: true
end
